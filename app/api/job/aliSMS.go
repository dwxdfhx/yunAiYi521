package job

import (
	"fmt"
	"github.com/gogf/gf/encoding/gjson"
	"github.com/gogf/gf/encoding/gurl"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/os/gproc"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/text/gstr"
	"github.com/golang/glog"
	"time"
	"yunAIYI/app/model/ai_history"
	"yunAIYI/app/model/ai_jobs"
)

//短信入口 目前两张:模拟发短信  急速数据接口发短信
func SendIndex(list []*ai_jobs.Entity) {
	glog.Info("满足条件的任务:", len(list))

	//SendSMSMONI(list)
	SendSMS(list)

}
func SendSMS(list []*ai_jobs.Entity) {
	//发短信核心
	for i := 0; i < len(list); i++ {
		go ApiSend_jisushuju(list[i].AddUserPhone, list[i].UserFromName, list[i].UserToName, list[i].UserToPhone, list[i].Smstype)
	}
}
func SendSMSMONI(list []*ai_jobs.Entity) {
	//串联发送
	for i := 0; i < len(list); i++ {
		time.Sleep(5 * time.Second)
		shellSEND(list[i].AddUserPhone, list[i].UserFromName, list[i].UserToName, list[i].UserToPhone, list[i].Smstype)
	}
}

//急速数据api
func ApiSend_jisushuju(phone, user_from_name, user_to_name, user_to_phone string, smstype int) {
	glog.Info("正在发送短信:", phone, user_from_name, user_to_name, user_to_phone)

	APPKEY := "APPKEY" // 你的appkey
	URL := "https://api.jisuapi.com/sms/send"
	mobile := user_to_phone // 手机号
	content := fmt.Sprintf("我是%s,致最%s【云爱伊】", user_from_name, "可爱的"+user_to_name+":"+getsms(smstype))
	url := URL + "?mobile=" + mobile + "&content=" + gurl.Encode(content) + "&appkey=" + APPKEY
	ghttp.PostContent(url)

	saveHistory(phone, user_to_phone, content)
}

func shellSEND(phone, user_from_name, user_to_name, user_to_phone string, smstype int) {

	content := fmt.Sprintf("[云爱伊]我是%s,致最可爱的%s:%s", user_from_name, user_to_name, getsms(smstype))

	abs := gfile.Pwd()
	abs = abs + gfile.Separator + "document" + gfile.Separator + "ADB" + gfile.Separator + "adb.exe"
	gproc.ShellRun(abs + " " + " shell am start -a android.intent.action.SENDTO -d sms:" + user_to_phone + " --es sms_body " + content)
	gproc.ShellRun(abs + " " + " shell input keyevent 61 input keyevent 61")
	gproc.ShellRun(abs + " " + " shell input keyevent 66")
	glog.Info("短信发送成功!", user_from_name, user_to_name, user_to_phone)

	saveHistory(phone, user_to_phone, content)
}
func saveHistory(phone, user_to_phone, content string) {
	hi := ai_history.Entity{Phone: phone, UserToPhone: user_to_phone, Md5Sms: content, Date: gtime.Now().String()}
	hi.Save()
	glog.Info(content, "已保存发送历史记录")
}
func getsms(smstype int) string {
	if smstype == 1 {
		return getSms_mingju()
	} else if smstype == 2 {
		return getSms_tuweiqinghua()
	} else {
		glog.Error("尚未设置短信类型")
		return ""
	}
}

func getSms_tuweiqinghua() string {
	for {
		content := ghttp.GetContent("https://api.lovelive.tools/api/SweetNothings")
		if gstr.Contains(content, "她") {
			glog.Info("当前内容不合适")
		} else {
			return content
		}
	}
}

type MinJU struct {
	txt    string `json:"txt"`
	status int    `json:"status"`
}

func getSms_mingju() string {
	content := ghttp.GetContent("https://data.zhai78.com/openOneGood.php")
	//mj := MinJU{}

	j, err := gjson.DecodeToJson([]byte(content))
	if err != nil {
		glog.Error(err)
		return ""
	} else {
		//fmt.Println("John Score:", j.GetString("txt"))
		return j.GetString("txt")
	}
}
