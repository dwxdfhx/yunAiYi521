package job

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gcron"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
	"github.com/golang/glog"
	"yunAIYI/app/model/ai_jobs"
)

func StartMainJob_scan() {
	//每隔20秒扫描一次
	gcron.AddSingleton("*/20 * * * * *", func() {
		glog.Info("开始扫描任务:", gtime.Now().String())

		db := g.DB()
		var list []*ai_jobs.Entity
		//state 1表示任务有效，0表示任务无效但未删除 -1表示删除
		//每天该任务执行状态 0未执行 1已执行
		all, _ := db.Table("ai_jobs").Where("state=?", 1).And("status=?", 0).Order("id desc").All()
		err := all.Structs(&list)
		if err != nil {
			glog.Error(err)
		} else {
			//listcheck := glist.New(true)
			var list2 []*ai_jobs.Entity

			for i := 0; i < len(list); i++ {
				//发信人姓名 收信人姓名 收信人电话
				// 使用 adb shell发送 依次发送
				// go SendSMS(list[i].UserFromName, list[i].UserToName, list[i].UserToPhone)
				//
				//listcheck.PushBack(list[i])
				if list[i].Time == hm() {
					list2 = append(list2, list[i])
					//当前任务满足条件 执行状态改为已执行
					db.Update("ai_jobs", "status=1", "id=?", list[i].Id)
				}
			}

			SendIndex(list2)

		}

	}, "scan")
}
func hm() string {
	hi := gtime.Now()
	h := gconv.String(hi.Hour())
	m := gconv.String(hi.Minute())
	if len(h) == 1 {
		h = "0" + h
	}
	if len(m) == 1 {
		m = "0" + m
	}
	return h + ":" + m
}
func StartMainJob_clear() {
	//23:59:59 清空任务日志
	gcron.AddSingleton("59 59 23 * * *", func() {
		glog.Info("开始清理任务:", gtime.Now().String())

		db := g.DB()
		_, err := db.Update("ai_jobs", "status=0", "state!=-1")
		if err != nil {
			glog.Error(gtime.Now().String(), "清理日志状态异常:", err)
		} else {
			glog.Info(gtime.Now().String(), "清理日志状态成功")
		}
	}, "clear")
}

func Init() {
	glog.Info("开始加装定时任务...")
	StartMainJob_scan()
	StartMainJob_clear()
}
