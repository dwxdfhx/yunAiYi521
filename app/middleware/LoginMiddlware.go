package middleware

import (
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/glog"
)

/**
拦截根路径的中间件
*/
const SESSIONKEY = "UserKey_SESSION"

var URLFILTER []string = []string{"/", "/Login", "/LoginOut"}

func MiddlewareAuth(r *ghttp.Request) {
	next := false
	//url相对路径
	path := r.Request.URL.Path
	glog.Info("当前路径:", path)
Loop:
	for _, v := range URLFILTER {
		if v == path {
			next = true
			break Loop
		}
	}

	if next {
		//非拦截请求
		r.Middleware.Next()
	} else {
		if r.Session.GetString(SESSIONKEY) == "" {
			r.Response.RedirectTo("/Login")
		} else {
			//已登录
			r.Middleware.Next()
		}
	}

}
