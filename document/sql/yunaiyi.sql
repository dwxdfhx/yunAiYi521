/*
Navicat MySQL Data Transfer

Source Server         : 10.10.10.3
Source Server Version : 80017
Source Host           : 10.10.10.3:3306
Source Database       : yunaiyi

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2020-03-20 16:27:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_history
-- ----------------------------
DROP TABLE IF EXISTS `ai_history`;
CREATE TABLE `ai_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL COMMENT '发信人电话',
  `user_to_phone` varchar(255) DEFAULT NULL,
  `md5sms` text,
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=688 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for ai_history_temp
-- ----------------------------
DROP TABLE IF EXISTS `ai_history_temp`;
CREATE TABLE `ai_history_temp` (
  `id` int(20) NOT NULL DEFAULT '0',
  `user_to_phone` varchar(255) DEFAULT NULL,
  `md5sms` text,
  `date` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for ai_jobs
-- ----------------------------
DROP TABLE IF EXISTS `ai_jobs`;
CREATE TABLE `ai_jobs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `add_user_phone` varchar(255) DEFAULT NULL COMMENT '添加该任务的用户phone',
  `user_from_name` varchar(255) DEFAULT NULL,
  `user_to_name` varchar(255) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `user_to_phone` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '0' COMMENT '每天该任务执行状态 0未执行 1已执行',
  `state` int(1) DEFAULT '1' COMMENT '任务添加默认为1 1表示任务有效，0表示任务无效但未删除 -1表示删除',
  `smstype` int(1) DEFAULT '1' COMMENT ' 1:名言警句      2:土味情话',
  `location` varchar(255) DEFAULT NULL COMMENT '手机号归属地',
  PRIMARY KEY (`id`),
  KEY `fpk_phone` (`add_user_phone`),
  CONSTRAINT `fpk_phone` FOREIGN KEY (`add_user_phone`) REFERENCES `ai_user` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for ai_user
-- ----------------------------
DROP TABLE IF EXISTS `ai_user`;
CREATE TABLE `ai_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstuse_time` varchar(255) DEFAULT NULL,
  `lastuse_time` varchar(255) DEFAULT NULL,
  `sms_count` int(11) DEFAULT '0',
  `type` int(1) DEFAULT '0' COMMENT '0普通会员  1付费会员',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for ai_words
-- ----------------------------
DROP TABLE IF EXISTS `ai_words`;
CREATE TABLE `ai_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL COMMENT '用户手机号码',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '自定义分类',
  `text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '自定义短信内容',
  `state` int(1) DEFAULT '1' COMMENT '有效1 无效 0',
  `time` varchar(255) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
