package main

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/gfile"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/os/gsession"
	"time"
	"yunAIYI/app/api/job"
	"yunAIYI/app/util"
	_ "yunAIYI/boot"
	_ "yunAIYI/router"
)

func main() {
	glog.Info("启动服务")
	index()
}

func index() {

	shttps := ghttp.GetServer()

	shttps.SetServerRoot("./public")
	shttps.SetSessionMaxAge(6 * time.Hour)
	shttps.SetSessionStorage(gsession.NewStorageMemory())

	shttps.SetPort(util.GetPort())

	//todo:打包时候才打开，dev模式必须注释，否则和本地443冲突
	//initHTTPS(shttps)

	initConfig()
	initJobs()
	autoDO()

	shttps.Run()
}

func initHTTPS(shttps *ghttp.Server) {
	sfile, kfile := loadHttps()
	shttps.EnableHTTPS(sfile, kfile)
	shttps.SetHTTPSPort(443)
}
func loadHttps() (string, string) {
	abs := gfile.Pwd()
	keyfile := abs + gfile.Separator + "document" + gfile.Separator + "www.homeapp.top.key"
	sfile := abs + gfile.Separator + "document" + gfile.Separator + "www.homeapp.top.pem"
	return sfile, keyfile
}
func initJobs() {
	glog.Info("初始化定时任务")
	job.Init()
}
func initConfig() {
}
func autoDO() {
	//自动更新系统时间
	//go util.UpdateSystemDateAuto()
	util.OpenUrl("https://localhost:" + g.Cfg().GetString("server.Address"))
	util.OpenUrl("http://localhost:" + g.Cfg().GetString("server.Address"))

}
