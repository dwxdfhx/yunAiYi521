package router

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"yunAIYI/app/api/home"
	"yunAIYI/app/api/job"
	"yunAIYI/app/middleware"
)

func init() {
	s := g.Server()
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.Middleware(middleware.MiddlewareAuth)
		group.ALL("/", home.Login)
		group.ALL("/Login", home.Login)
		group.ALL("/Main", home.Main)
		group.ALL("/User", home.User)
		group.ALL("/Manager", home.Manager)
		group.ALL("/MyHistory", home.MyHistory)

		group.ALL("/LoginOut", home.LoginOut)
		group.ALL("/ShowSesssion", home.ShowSesssion)
		group.ALL("/PageHistory", home.PageHistory)

		group.ALL("/Page", job.Page)
		group.ALL("/AddOrUpdateJobPage", job.AddOrUpdateJobPage)
		group.ALL("/DeleteJob", job.DeleteJob)
		group.ALL("/RunOne", job.RunOne)
		group.ALL("/AddOne", job.AddOne)
		group.ALL("/ChangeState", job.ChangeState)

	})
}
